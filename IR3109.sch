EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-4ALL-cache
EELAYER 25 0
EELAYER END
$Descr A1 33110 23386
encoding utf-8
Sheet 23 411
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 9150 9825 0    60   Input ~ 0
16-v+
Text HLabel 9150 9925 0    60   Input ~ 0
15-out-4
Text HLabel 9150 10025 0    60   Input ~ 0
14-c-4
Text HLabel 9150 10125 0    60   Input ~ 0
13-in-4
Text HLabel 9150 11425 0    60   Input ~ 0
1-gnd
Text HLabel 9150 10325 0    60   Input ~ 0
12-out-3
Text HLabel 9150 10425 0    60   Input ~ 0
11-c-3
Text HLabel 9150 10525 0    60   Input ~ 0
10-in-3
Text HLabel 9150 10625 0    60   Input ~ 0
9-control-in
Text HLabel 9150 10725 0    60   Input ~ 0
8-v-
Text HLabel 9150 10825 0    60   Input ~ 0
7-out-2
Text HLabel 9150 10925 0    60   Input ~ 0
6-c-2
Text HLabel 9150 11025 0    60   Input ~ 0
5-in-2
Text HLabel 9150 11125 0    60   Input ~ 0
4-out-1
Text HLabel 9150 11225 0    60   Input ~ 0
3-c-1
Text HLabel 9150 11325 0    60   Input ~ 0
2-in-1
$EndSCHEMATC
