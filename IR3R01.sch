EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-4ALL-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 411
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3675 2775 0    60   Input ~ 0
1-mod
Text HLabel 3675 3000 0    60   Input ~ 0
2-a
Text HLabel 3675 3250 0    60   Input ~ 0
3-d
Text HLabel 3675 3475 0    60   Input ~ 0
4-r
Text HLabel 3700 3675 0    60   Input ~ 0
5-s
Text HLabel 3625 3900 0    60   Input ~ 0
6-com
Text HLabel 4350 2800 0    60   Input ~ 0
7-ref
Text HLabel 4500 3000 0    60   Input ~ 0
8-v-
Text HLabel 4450 3225 0    60   Input ~ 0
9-gnd
Text HLabel 4425 3450 0    60   Input ~ 0
10-cap
Text HLabel 4425 3650 0    60   Input ~ 0
11-out
Text HLabel 4425 3925 0    60   Input ~ 0
12-cur-in
Text HLabel 4400 4125 0    60   Input ~ 0
13-gate
Text HLabel 5025 2800 0    60   Input ~ 0
14-trig
Text HLabel 5000 3000 0    60   Input ~ 0
15-ret
Text HLabel 5025 3275 0    60   Input ~ 0
16-v+
$EndSCHEMATC
